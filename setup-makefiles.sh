#!/bin/bash
#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017-2018 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

set -e

export INITIAL_COPYRIGHT_YEAR=2017
export VENDOR=gapps
MY_DIR=$(realpath $(dirname $0))
LINEAGE_ROOT=$MY_DIR/../..
HELPER=$LINEAGE_ROOT/vendor/carbon/build/tools/extract_utils.sh

# Load extract_utils and do some sanity checks
if [ ! -f $HELPER ]; then
    echo "Unable to find helper script at $HELPER"
    exit 1
fi
. $HELPER

# Initialize the helper
setup_vendor arm64 $VENDOR $LINEAGE_ROOT true

# Copyright headers
write_headers arm64

write_makefiles $MY_DIR/proprietary-files-arm64.txt

sed -i 's/TARGET_DEVICE/TARGET_ARCH/g' $ANDROIDMK

# Make Google SuW override Provision
sed -i 's/\(SetupWizard.apk\)/\1\nLOCAL_OVERRIDES_PACKAGES := Provision/' $ANDROIDMK

# We are done
write_footers
