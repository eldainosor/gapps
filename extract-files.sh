#!/bin/bash
#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017-2018 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

set -e

export VENDOR=gapps
MY_DIR=$(realpath $(dirname $0))
LINEAGE_ROOT=$MY_DIR/../..
HELPER=$LINEAGE_ROOT/vendor/carbon/build/tools/extract_utils.sh

# Load extract_utils and do some sanity checks
if [ ! -f $HELPER ]; then
    echo "Unable to find helper script at $HELPER"
    exit 1
fi
. $HELPER

SRC=$1

if [ -z $SRC ]; then
    echo "Path to system dump not specified! Specify one with --path"
    exit 1
fi

# Initialize the helper for gapps, export EXTRACT_ARCH to architecture
# other than common to extract something else.
setup_vendor arm64 $VENDOR $LINEAGE_ROOT

extract $MY_DIR/proprietary-files-arm64.txt $SRC

$MY_DIR/setup-makefiles.sh
