#!/bin/bash
# (c) Joey Rizzoli, 2015
# (c) Paul Keith, 2017
# Released under GPL v2 License

##
# var
#
DATE=$(date -u +%Y%m%d-%H%M)
TOP=$(realpath .)
ANDROIDV=9.0.0
OUT=$TOP/out
BUILD=$TOP/build
METAINF=$BUILD/meta
GLOG=$TOP/gapps_log
ADDOND=$TOP/addond.sh
[[ -z $BUILDSTATUS ]] && BUILDSTATUS=UNOFFICIAL
ZIPNAME=ShagGApps-$BUILDSTATUS-$ANDROIDV-arm64-$DATE.zip

##
# functions
#
function clean() {
    echo "Cleaning up..."
    rm -r $OUT/arm64
    rm /tmp/$ZIPNAME
    return $?
}

function failed() {
    echo "Build failed, check $GLOG"
    exit 1
}

function create() {
    test -f $GLOG && rm -f $GLOG
    echo "Starting GApps compilation" > $GLOG
    echo "ARCH= arm64" >> $GLOG
    echo "OS= $(uname -s -r)" >> $GLOG
    echo "NAME= $(whoami) at $(uname -n)" >> $GLOG
    PREBUILT=$TOP/arm64/proprietary
    test -d $OUT || mkdir $OUT;
    test -d $OUT/arm64 || mkdir -p $OUT/arm64
    test -d $OUT/arm64/system || mkdir -p $OUT/arm64/system
    echo "Build directories are now ready" >> $GLOG
    echo "Getting prebuilts..."
    echo "Copying stuff" >> $GLOG
    cp -r $PREBUILT/* $OUT/arm64/system >> $GLOG
    echo "Copying RRO overlays..." >> $GLOG
    mkdir -p $OUT/arm64/system/product/overlay >> $GLOG
    for i in $(find overlay/ | grep ShagGAppsOverlay*); do
        cp $i $OUT/arm64/system/product/overlay >> $GLOG
    done
    echo "Generating addon.d script" >> $GLOG
    test -d $OUT/arm64/system/addon.d || mkdir -p $OUT/arm64/system/addon.d
    cp -f addond_head $OUT/arm64/system/addon.d
    cp -f addond_tail $OUT/arm64/system/addon.d
}

function zipit() {
    echo "Importing installation scripts..."
    test -d $OUT/arm64/META-INF || mkdir $OUT/arm64/META-INF;
    cp -r $METAINF/* $OUT/arm64/META-INF/ && echo "Meta copied" >> $GLOG
    echo "Creating package..."
    cd $OUT/arm64
    zip -r /tmp/$ZIPNAME . >> $GLOG
    rm -rf $OUT/tmp >> $GLOG
    cd $TOP
    if [ -f /tmp/$ZIPNAME ]; then
        echo "Signing zip..."
        java -Xmx2048m -jar $TOP/build/sign/signapk.jar -w $TOP/build/sign/testkey.x509.pem $TOP/build/sign/testkey.pk8 /tmp/$ZIPNAME $OUT/$ZIPNAME >> $GLOG
    else
        echo "Couldn't zip files!"
        echo "Couldn't find unsigned zip file, aborting" >> $GLOG
        return 1
    fi
}

function getmd5() {
    MD5SUMFILE=$OUT/$ZIPNAME-md5sum.txt
    if [ -x $(which md5sum) ]; then
        echo "md5sum is installed, getting md5..." >> $GLOG
        echo "Getting md5sum..."
        GMD5=$(md5sum $OUT/$ZIPNAME | cut -d ' ' -f 1)
        printf "$GMD5" > $MD5SUMFILE
        printf "\n\n" >> $MD5SUMFILE #print newline so it's recognised as a text file
        echo "md5 exported at $MD5SUMFILE"
        return 0
    else
        echo "md5sum is not installed, aborting" >> $GLOG
        return 1
    fi
}

function createchangelog() {
    CHANGELOGFILE=$OUT/$ZIPNAME-changelog.txt
    if ! LASTBUILDDATE=$(cat $TOP/.last_build 2>/dev/null); then
        echo "$TOP/.last_build not found, not generating changelog."
    elif [[ $BUILDSTATUS = UNOFFICIAL ]]; then
        echo "Build status is unofficial, not generating a changelog."
    else
        CHANGELOG="$(git log --pretty='%ad %s %n' --date=short --since="$LASTBUILDDATE")"
        if [[ -n $CHANGELOG ]]; then
            echo "Generating changelog..." | tee -a $GLOG
            printf "Changes since $LASTBUILDDATE:\n\n" > $CHANGELOGFILE
            printf "$CHANGELOG" >> $CHANGELOGFILE
            printf "\n\n" >> $CHANGELOGFILE #print a newline so it's recognised as a txt file
        else
            echo "No changes were found, not generating a changelog." | tee -a $GLOG
        fi
    fi
}

##
# main
#
if [ -x $(which realpath) ]; then
    echo "Realpath found!" >> $GLOG
else
    TOP=$(cd . && pwd) # some darwin love
    echo "No realpath found!" >> $GLOG
fi

for func in create zipit getmd5 clean createchangelog; do
    $func
    ret=$?
    if [ "$ret" == 0 ]; then
        continue
    else
        failed
    fi
done

echo "Done!" >> $GLOG
echo
echo "Build completed!"
echo "---------------------------------------"
echo "md5sum: $GMD5"
echo "Package: $OUT/$ZIPNAME"
[[ $BUILDSTATUS != UNOFFICIAL ]] && echo "$(date)" > $TOP/.last_build
unset BUILDSTATUS
exit 0
